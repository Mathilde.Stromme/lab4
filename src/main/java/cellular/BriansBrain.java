package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;
    
    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }

    @Override
	public int numberOfRows() {
		//TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		//TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for(int row=0; row<numberOfRows(); row++){
			for(int col=0; col<numberOfColumns(); col++){
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration=nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		int neighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState current = getCellState(row, col);
		if(current == CellState.ALIVE){
            return CellState.DYING;
		} else if (current==CellState.DYING){
            return CellState.DEAD;
        } else if (current==CellState.DEAD){
            if (neighbors==2){
                return CellState.ALIVE;
            }
        }
        
		return CellState.DEAD;
	}

    private int countNeighbors(int row, int col, CellState state) {
		int count = 0;
		for(int r = row - 1; r <= row + 1; r++){
			if(r<0 || r >= numberOfRows()) continue;
			for(int c = col -1; c<= col +1; c++){
				if(c<0 || c>= numberOfColumns()) continue;
				if(r==row && c==col) continue;

				CellState current = getCellState(r, c);
				if (current==state){
					count++;
				}
			}
		}
		return count;}
	


    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
    }

    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return currentGeneration;
    }
}
